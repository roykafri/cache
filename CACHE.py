import functools

c = {}


def cache(func):
    @functools.wraps(func)
    def wrapper(*args):
        print(args)
        if args in c.keys():
            return c[args]
        else:
            c[args] = func(*args)
            print("added" + str(args))
            return c[args]
    return wrapper


@cache
def greet(name: str) -> str:
    print("running greet")
    return ("Hey {}".format(name))


def main():
    a = greet("Harel")
    print(a)
    print("\n")
    a = greet("Harel")
    print(a)
    print("\n")
    a = greet("H")
    print(a)

if __name__ == '__main__':
    main()